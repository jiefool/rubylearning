def repeat(duration, interval)
  1.upto(duration) do |x|
    yield
    sleep(interval)
  end
end

repeat(60, 10) { puts `ls` }

