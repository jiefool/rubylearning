# require 'profile'
puts "Using profile from Ruby, running: ruby -rprofile random_number_distribution.rb"
puts "without to_s"
# RubyProf.start
counts = Hash.new 0
100_000.times do
  number = (rand - rand).round(1)
  counts[number] += 1
end
counts.sort.each{ |key, value| puts "#{key}: #{value}"}

# result = RubyProf.stop
# printer = RubyProf::FlatPrinter.new(result)
# printer.print(STDOUT)

__END__

  %   cumulative   self              self     total
 time   seconds   seconds    calls  ms/call  ms/call  name
 44.31    19.31     19.31   100021     0.19     0.41  nil#
 15.99    26.28      6.97   100000     0.07     0.09  Hash#[]=
 13.93    32.35      6.07   100000     0.06     0.08  Hash#[]
  5.87    34.91      2.56        1  2560.00 43570.00  Integer#times
  4.86    37.03      2.12   200000     0.01     0.01  Float#hash
  4.06    38.80      1.77   200000     0.01     0.01  Kernel#rand
  3.97    40.53      1.73   199958     0.01     0.01  Float#eql?
  2.68    41.70      1.17   100000     0.01     0.01  Float#-
  2.23    42.67      0.97   100000     0.01     0.01  Float#round
  2.07    43.57      0.90   100000     0.01     0.01  Fixnum#+
  0.02    43.58      0.01       68     0.15     0.15  Array#<=>
  0.00    43.58      0.00        1     0.00     0.00  TracePoint#enable
  0.00    43.58      0.00        1     0.00     0.00  Class#new
  0.00    43.58      0.00       23     0.00     0.00  Kernel#puts
  0.00    43.58      0.00       23     0.00     0.00  IO#puts
  0.00    43.58      0.00        1     0.00     0.00  Hash#initialize
  0.00    43.58      0.00       46     0.00     0.00  IO#write
  0.00    43.58      0.00        2     0.00     0.00  IO#set_encoding
  0.00    43.58      0.00        1     0.00     0.00  MonitorMixin#mon_exit
  0.00    43.58      0.00        1     0.00     0.00  Mutex#unlock
  0.00    43.58      0.00        1     0.00     0.00  MonitorMixin#mon_check_owner
  0.00    43.58      0.00        1     0.00     0.00  MonitorMixin#mon_enter
  0.00    43.58      0.00        1     0.00     0.00  Mutex#lock
  0.00    43.58      0.00        1     0.00     0.00  Hash#each
  0.00    43.58      0.00       68     0.00     0.00  Float#<=>
  0.00    43.58      0.00        3     0.00     0.00  Thread.current
  0.00    43.58      0.00        1     0.00    10.00  Enumerable#sort
  0.00    43.58      0.00       21     0.00     0.00  Float#to_s
  0.00    43.58      0.00       21     0.00     0.00  Fixnum#to_s
  0.00    43.58      0.00        1     0.00     0.00  Array#each
  0.00    43.58      0.00        1     0.00     0.00  TracePoint#disable
  0.00    43.58      0.00        1     0.00 43580.00  #toplevel
Using profile from Ruby, running: ruby -rprofile random_number_distribution.rb
without to_s
-1.0: 126
-0.9: 957
-0.8: 2069
-0.7: 2981
-0.6: 4006
-0.5: 5011
-0.4: 5979
-0.3: 7062
-0.2: 7921
-0.1: 9066
0.0: 9735
0.1: 9040
0.2: 8115
0.3: 6939
0.4: 5851
0.5: 4973
0.6: 4127
0.7: 2951
0.8: 1943
0.9: 1003
1.0: 145
