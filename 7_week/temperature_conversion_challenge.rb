def is_numeric?(input)
  /\A[-+]?[0-9]*\.?[0-9]+\Z/.match(input)
end

def selection_valid?(selection, length)
  if is_numeric?(selection)
    selection.to_i <= length
  else
    exit if /[eE]/.match(selection)
  end
end

def selection_action(valid)
  unless valid
    puts "Please use a valid selection."
  end
end

def prompt(message)
  print message
  gets.chomp
end

def validation_action(unit_from,length)
  selection_action(selection_valid?(unit_from, length))
  selection_valid?(unit_from, length)
end

def display(display_text)
  display_text.length.times{ print '='}
  puts ''
  puts display_text
  display_text.length.times{ print '='}
  puts ''
end

if __FILE__ == $PROGRAM_NAME
  require_relative 'temperature_converter'
  available_units = TemperatureConverter::AVAILABLE_UNITS
  tc = TemperatureConverter.new
  while true do # yuck, use loop do if you really need an infinite loop
    available_units.each_with_index do |value, index|
      puts "[#{index+1}] #{value}"
    end
    puts "[E] Exit"
    unit_from = prompt("Enter unit from: ")
    next unless validation_action(unit_from, available_units.length)
    unit_to = prompt("Enter unit to: ")
    next unless validation_action(unit_to, available_units.length)
    value_to_convert = prompt("Enter value to convert: ").chomp
    if is_numeric?(value_to_convert)
      result =  tc.convert(available_units[unit_from.to_i - 1], available_units[unit_to.to_i - 1], value_to_convert.to_f)
      display_text =  "#{value_to_convert} #{available_units[unit_from.to_i - 1]} is #{result} #{available_units[unit_to.to_i - 1]}"
      display(display_text)
    else
      puts "Please enter a numeric value."
      next
    end
  end
end

