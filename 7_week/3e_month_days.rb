require 'date'
# doctest: number of days of a given month
# >> month_days(5)
# => 31
# >> month_days(2, 2000)
# => 29
def month_days(month, year = Time.new.year)
  (Date.parse("1/#{month}/#{year}").next_month - 1).day
end

if __FILE__ == $PROGRAM_NAME 
  puts month_days(5)
  puts month_days(2, 2000)
end

