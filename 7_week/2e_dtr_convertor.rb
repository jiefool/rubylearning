class DTRConverter
  # doctest: return conversion in 40 rate
  # >> dtr = DTRConverter.new
  # >> dtr.convert 100
  # => 4000
  def convert(dollar_amount)
    dollar_amount * 40
  end
end

class FixedDTRConverter < DTRConverter
  # doctest: return conversion in 38 rate
  # >> dtr2 = FixedDTRConverter.new
  # >> dtr2.convert(100)
  # => 3800
  def convert(dollar_amount)
    dollar_amount * 38
  end
end

if __FILE__ == $PROGRAM_NAME
  dtr1 = DTRConverter.new
  puts dtr1.convert(100)

  dtr2 = FixedDTRConverter.new
  puts dtr2.convert(100)
end

