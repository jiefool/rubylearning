def last_modified(file)
  seconds_to_days(Time.new - File.ctime(file))
end

def seconds_to_days(seconds)
  seconds / (24*60*60)
end
if __FILE__ == $PROGRAM_NAME
  files = Dir.glob('**/*')
  files.each do |file|
    puts "#{file} was last modified #{last_modified(file).round(2)} days ago."
  end
end

