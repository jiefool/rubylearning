class Klass
	def initialize(hello)
		@hello = hello
	end

	def say_hello
		puts "Hello #@hello"
	end
end
