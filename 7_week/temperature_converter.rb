class TemperatureConverter

  AVAILABLE_UNITS = %w(celsius fahrenheit kelvin reaumur planck rankine newton romer delisle)

  def convert(unit_from, unit_to, value_to_convert)
    return value_to_convert if unit_from == 'kelvin' && unit_to == 'kelvin'
    kelvin = unit_from == 'kelvin' ? value_to_convert : send('X_to_kelvin'.sub('X', unit_from), value_to_convert)
    return kelvin if unit_to == 'kelvin'
    send('kelvin_to_X'.sub('X', unit_to), kelvin)
  end

  private
  # other units to kelvin
  def celsius_to_kelvin(value) value + 273.15 end
  def fahrenheit_to_kelvin(value) (0.555556 * value) + 255.372 end
  def reaumur_to_kelvin(value) (1.25 * value) + 273.15 end
  def planck_to_kelvin(value) 1.416785E+32 * value end
  def rankine_to_kelvin(value) 0.555556 * value end
  def newton_to_kelvin(value) (3.0303 * value) + 273.15 end
  def romer_to_kelvin(value) (1.90476 * value) + 258.864 end
  def delisle_to_kelvin(value) ( value / -1.49999925) + 373.15 end
  # kelvin to other units
  def kelvin_to_celsius(value) value - 273.15 end
  def kelvin_to_fahrenheit(value) (1.8*value) - 459.67 end
  def kelvin_to_reaumur(value) (0.8 * value) - 218.52 end
  def kelvin_to_planck(value) value / 1.416785E+32 end
  def kelvin_to_rankine(value) 1.8 * value end
  def kelvin_to_newton(value) (0.33 * value) - 90.1395 end
  def kelvin_to_romer(value) (0.525 * value) - 135.904 end
  def kelvin_to_delisle(value) (value / -0.666666667) + 559.725 end


end
