require 'yaml'
require_relative 'lesson7exercise1'

if __FILE__ == $PROGRAM_NAME
  klass = Klass.new('World')
  klass.say_hello

  # marshalling the klass
  puts 'Serializing and saving klass to file'
  File.open('klass', 'w+') do |f|
    Marshal.dump(klass, f)
  end
  puts '==================================='
  puts 'Loading the klass file'
  loaded_klass = nil
  File.open('klass') do |f|
    loaded_klass = Marshal.load(f)
  end
  loaded_klass.say_hello if loaded_klass.respond_to? 'say_hello'
  # using yml dump and load
  # dumping as yml
  File.open('klass_yaml', 'w+') do |f|
    YAML.dump(klass, f)
  end
  puts '=================================='
  puts 'Loading the klass from yaml file'
  loaded_klass_yaml = nil
  File.open('klass_yaml') do |f|
    loaded_klass_yaml = YAML.load(f)
  end
  loaded_klass_yaml.say_hello if loaded_klass_yaml.respond_to? 'say_hello'
end
