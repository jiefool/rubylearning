class UnpredictableString < String
  def scramble
    split('').shuffle!.join
  end

  def to_s
    scramble
  end
end

t = UnpredictableString.new('It was a dark and stormy night')
10.times do
  puts t
end

