  # doctest: odd or even
  # >> odd? 12
  # => false
  # >> odd? 7
  # => true
  def odd?(number)
    number % 2 == 1
  end 

  # doctest: prints number type
  # >> num_type 12
  # => "12 is even"
  # >> num_type 7
  # => "7 is odd"
  def number_parity_prompt(number)
    "#{number} is #{number_parity number}"
  end

  # doctest: type of number
  # >> type 12
  # => 'even'
  # >> type 7
  # => 'odd'
  def number_parity(number)
    odd?(number) ? 'odd' : 'even'
  end

  if __FILE__ == $PROGRAM_NAME
    [12, 23, 456, 123, 4579].each do |number|
      puts number_parity_prompt number
    end
  end