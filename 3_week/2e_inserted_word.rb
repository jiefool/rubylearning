# doctest: replace text in a file by new text
def edit_file(text_file, from_text, to_text)
  if File.exist?(text_file)
    string = edit_string(File.read(text_file), from_text, to_text)
    write_to_file(text_file, string)
  else
    puts "File don't exist."
  end
end

# doctest: edit string
# >> edit_string 'word text foreword text wordworth text word', 'word', 'inserted word'
# => 'inserted word text foreword text wordworth text inserted word'
def edit_string(string, from_text, to_text)
  string.gsub(/^#{from_text}|[^\w\n]#{from_text}[^\w]|#{from_text}$/, "#{to_text}")
end

# doctest: write_to_file
# >> write_to_file 'sample_file.txt', 'content of sample file'
# >> File.read('sample_file.txt')
# => "content of sample file\n"
def write_to_file(text_file, string)
  output_file = File.open(text_file, 'w')
  output_file.puts(string)
  output_file.close
end

if __FILE__ == $PROGRAM_NAME
  puts "usage: ruby 2e_inserted_word.rb text_file 'word' 'inserted'"
  edit_file(ARGV[0], ARGV[1], ARGV[2]) unless ARGV[0].nil? && ARGV[1].nil? && ARGV[2].nil?
end
