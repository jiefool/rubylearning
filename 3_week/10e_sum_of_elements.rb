# doctest: reduce enumerable item
# >> calculate_sum [1, 2, 3, 4, 5]
# => 15
def calculate_sum(enumerable)
  enumerable.reduce(:+)
end
 
if __FILE = $PROGRAM_NAME
  puts calculate_sum([1, 2, 3, 4, 5])	
end

