# rubocop:disable Style/BlockComments
=begin
Write a Ruby program that prints the numbers from 1 to 100. But for multiples
of three print "Fizz" instead of the number and for multiples of five print
"Buzz". For numbers which are multiples of both three and five print
"FizzBuzz". Discuss this here. Just click "Add a new discussion topic" and fill
out the title, and post your solution. I will post my solution as an example.
=end

# doctest: multiple of 3 return Fizz
# >> fizzbuzz 3
# => 'Fizz'
# doctest: multiple of 5 return Buzz
# >> fizzbuzz 5
# => 'Buzz'
# doctest: multiple of 3 and 5 return FizzBuzz
# >> fizzbuzz 15
# => 'FizzBuzz'
# doctest: return number if no multiple
# >> fizzbuzz 2
# => 2
def fizzbuzz(number)
  result = ''
  result << 'Fizz' if number % 3 == 0
  result << 'Buzz' if  number % 5 == 0
  result.empty? ? number : result
end

if __FILE__ == $PROGRAM_NAME
  (1..100).each do |number|
    puts fizzbuzz number
  end
end
