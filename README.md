# My RubyLearning Course Notes

I have decided to go through the popular Ruby Course at
[RubyLearning](http://rubylearning.org) and am keeping my notes and code
here.

Notes are in the wiki, and code of course in the folders.
