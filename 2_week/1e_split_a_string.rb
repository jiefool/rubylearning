# doctest: linify string
# >> linify "jay\npaul\naying\n"
# => "Line 1: jay\nLine 2: paul\nLine 3: aying\n"
def linify(string)
  linified = ''
  string.split(/\n/).map.with_index(1) do |item, index|
    linified << "Line #{index}: #{item}\n"
  end
  linified
end

if __FILE__ == $PROGRAM_NAME
  puts linify "I really\nwant to\nsplit this\nstring instead.\n"
end
