# rubocop:disable Style/BlockComments
=begin
Write a method leap_year?. It should accept a year value from the user, check
whether it's a leap year, and then return true or false. With the help of this
leap_year?() method calculate and display the number of minutes in a leap year
(2000 and 2004) and the number of minutes in a non-leap year (1900 and 2005).
Note: a century year, like 1900 and 2000, is a leap year only if it is
divisible by 400.
=end

# doctest: return true year 2000
# >> leap_year? 2000
# => true
# doctest: return true year 2004
# >> leap_year? 2004
# => true
# doctest: return false year is 1900
# >> leap_year? 1900
# => false
# doctest: false if year 2005
# >> leap_year? 2005
# => false
def leap_year?(year)
  year % 400 == 0 || year % 4 == 0 && year % 100 != 0
end


if __FILE__ == $PROGRAM_NAME
  many_years = Array.new(15) {rand(1800..2199)}
  many_years.push(1900, 1999, 2000, 2004)
  many_years.each do |year|
    puts "Year #{year} is a leap year? #{leap_year?(year)}"
  end
end

