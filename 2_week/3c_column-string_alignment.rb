# doctest: linify string
# >> linify "jay\npaul\naying\njay\npaul\naying\njay\npaul\naying\njay\npaul\naying\n"
# => "Line 01: jay\nLine 02: paul\nLine 03: aying\nLine 04: jay\nLine 05: paul\nLine 06: aying\nLine 07: jay\nLine 08: paul\nLine 09: aying\nLine 10: jay\nLine 11: paul\nLine 12: aying\n"
def linify(string)
  linified = ''
  mil = max_index_length(string.split(/\n/))
  string.split(/\n/).map.each.with_index(1) do |item, index|
  	icl = index_char_length(index)
  	linified << 'Line '
  	max_index_diff(mil, icl).times { linified << '0' }
    linified << "#{index}: #{item}\n"
  end
  linified
end

# doctest: string array length
# >> max_index_length ["jay", "paul", "aying"]
# => 1
def max_index_length(string_array)
	string_array.size.to_s.length
end

# doctest: index char length
# >> index_char_length 12
# => 2
def index_char_length(index)
	index.to_s.length
end

# doctest: max index and index char length difference
# >> max_index_diff 3, 1
# => 2
def max_index_diff(max, index)
	max - index
end

if __FILE__ == $PROGRAM_NAME
  puts linify "jay\npaul\naying\njay\npaul\naying\njay\npaul\naying\njay\npaul\naying\n"
end