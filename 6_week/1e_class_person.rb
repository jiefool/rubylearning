class Person
	attr_reader :balance
	
	# doctest: Setup
  # >> jay = Person.new
  # doctest: We have an instance variable named @balancd
  # >> jay.instance_variables
  # => [:@balance]
	def initialize
		@balance = 'balance'
	end	
end

person = Person.new
puts person.balance

Person2  = Struct.new(:balance)
person2 = Person2.new('balance2')
puts person2.balance