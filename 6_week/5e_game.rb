file = "game.sav"

def save_game(file)
  score = 1000
  open(file, "w") do |f|
	  f.puts(score)
	  f.puts(Time.new.to_i)
  end
  sleep(2)
end

def load_game(file)
	 if check_time?(file)
	 		puts "Your score is: #{File.readlines(file)[0]}"
	 else
	 		puts "I suspect you of cheating!"
	 end
end

def check_time?(file)
	File.readlines(file)[1].to_i == File.ctime(file).to_i
end

if __FILE__ == $PROGRAM_NAME
	# not cheating
	save_game(file)
	load_game(file)

	# cheating
	open(file, "r+b") { |f| f.write("9") }
	load_game(file) 
end
