class MP3ID3Tag
	attr_reader :title, :artist, :album, :year

	OFFSET = 128
	
	def initialize(mp3_file)
		string_tag = file_tail(MP3_FILE, OFFSET)
		file_info(string_tag)
	end

	def file_info(string_tag)
		unless string_tag.nil?
			@title = string_tag[3, 30]
			@artist = string_tag[33, 30]
			@album = string_tag[63, 30]
			@year = string_tag[93, 30]			
		end
	end

	def file_tail (file, offset)
    f=File.new(file)
    f.seek(-offset,IO::SEEK_END)
    f.read    
  end
end	

if __FILE__ == $PROGRAM_NAME
	MP3_FILE = 'song.mp3'
	mp3_info = MP3ID3Tag.new(MP3_FILE)
	puts mp3_info.title	
	puts mp3_info.artist	
	puts mp3_info.album	
	puts mp3_info.year		
end





