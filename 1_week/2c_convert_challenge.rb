# ****************************CELSIUS*************************************
# doctest: converts celsius to fahrenheit
# >> celsius_to_fahrenheit 100
# => 212
def celsius_to_fahrenheit(celsius)
  ((celsius * 9) / 5.0) + 32
end

# doctest: converts celcius to kelvin
# >> celsius_to_kelvin 100
# => 373.15
def celsius_to_kelvin(celsius)
  celsius + 273.15
end

# doctest: converts celcius to reaumur
# >> celsius_to_reaumur 100
# => 80
def celsius_to_reaumur(celsius)
  0.8 * celsius
end

# doctest: converts celsius to planck
# >> celsius_to_planck 100
# => 2.633780001905725e-30
def celsius_to_planck(celsius)
  (celsius / 1.416785e+32) + 1.92795660597762e-30
end

# doctest: converts celsius to rankine
# >> celsius_to_rankine 100
# => 671.67
def celsius_to_rankine(celsius)
  (((celsius * 9) / 5.0) + 491.67).round(8)
end

# doctest: converts celsius to newton
# >> celsius_to_newton 100
# => 33.0
def celsius_to_newton(celsius)
  0.33 * celsius
end

# doctest: converts celsius to romer
#
# >> celsius_to_romer 100
# => 60.0
def celsius_to_romer(celsius)
  (0.525 * celsius) + 7.5
end

# doctest: converts celsius to delisle
# >> celsius_to_delisle 100
# => 7.500000265281415e-08
def celsius_to_delisle(celsius)
  (celsius / -0.666666667) + 150
end

# ****************************FAHRENHEIT*************************************
# doctest: converts fahrenheit to celsius
# >> fahrenheit_to_celsius 100
# => 37.77777777777778
def fahrenheit_to_celsius(fahrenheit)
  ((fahrenheit - 32) * 5.0) / 9
end

# doctest: converts fahrenheit to kelvin
# >> fahrenheit_to_kelvin 100
# => 310.92777777777775
def fahrenheit_to_kelvin(fahrenheit)
  celsius_to_kelvin fahrenheit_to_celsius fahrenheit
end

# doctest: converts fahrenheit to reaumur
# >> fahrenheit_to_reaumur 100
# => 30.222222222222225
def fahrenheit_to_reaumur(fahrenheit)
  celsius_to_reaumur fahrenheit_to_celsius fahrenheit
end

# doctest: converts fahrenheit to planck
# >> fahrenheit_to_planck 100
# => 2.194600999994904e-30
def fahrenheit_to_planck(fahrenheit)
  celsius_to_planck fahrenheit_to_celsius fahrenheit
end

# doctest: converts fahrenheit to rankine
# >> fahrenheit_to_rankine 100
# => 559.67
def fahrenheit_to_rankine(fahrenheit)
  celsius_to_rankine fahrenheit_to_celsius fahrenheit
end

# doctest: converts fahrenheit to newton
# >> fahrenheit_to_newton 100
# => 12.466666666666667
def fahrenheit_to_newton(fahrenheit)
  celsius_to_newton fahrenheit_to_celsius fahrenheit
end

# doctest: converts fahrenheit to romer
# >> fahrenheit_to_romer 100
# => 27.333333333333336
def fahrenheit_to_romer(fahrenheit)
  celsius_to_romer fahrenheit_to_celsius fahrenheit
end

# doctest: converts fahrenheit to delisle
# >> fahrenheit_to_delisle 100
# => 93.33333336166666
def fahrenheit_to_delisle(fahrenheit)
  celsius_to_delisle fahrenheit_to_celsius fahrenheit
end

# doctest: converts newton to celsius
# >> newton_to_celsius 100
# => 303.03
def newton_to_celsius(newton)
  3.0303 * newton
end

# doctest: converts newton to fahrenheit
# >> newton_to_fahrenheit 100
# => 577.454
def newton_to_fahrenheit(newton)
  celsius_to_fahrenheit newton_to_celsius newton
end

# doctest: converts newton to kelvin
# >> newton_to_kelvin 100
# => 576.18
def newton_to_kelvin(newton)
  celsius_to_kelvin newton_to_celsius newton
end

# doctest: converts newton to reaumur
# >> newton_to_reaumur 100
# => 242.42399999999998
def newton_to_reaumur(newton)
  celsius_to_reaumur newton_to_celsius newton
end

# doctest: converts newton to planck
# >> newton_to_planck 100
# => 4.066813242658556e-30
def newton_to_planck(newton)
  celsius_to_planck newton_to_celsius newton
end

# doctest: converts newton to rankine
# >> newton_to_rankine 100
# => 1037.124
def newton_to_rankine(newton)
  celsius_to_rankine newton_to_celsius newton
end

# doctest: converts newton to romer
# >> newton_to_romer 100
# => 166.59074999999999
def newton_to_romer(newton)
  celsius_to_romer newton_to_celsius newton
end

# doctest: converts newton to delisle
# >> newton_to_delisle 100
# => -304.54499977
def newton_to_delisle(newton)
  (celsius_to_delisle newton_to_celsius newton).round(8)
end

# ****************************ROMER*************************************
# doctest: converts romer to celsius
# >> romer_to_celsius 100
# => 176.1903
def romer_to_celsius(romer)
  (1.90476 * romer) - 14.2857
end

# doctest: converts romer to fahrenheit
# >> romer_to_fahrenheit 100
# => 349.14254
def romer_to_fahrenheit(romer)
  celsius_to_fahrenheit romer_to_celsius romer
end

# doctest: converts romer to kelvin
# >> romer_to_kelvin 100
# => 449.34029999999996
def romer_to_kelvin(romer)
  celsius_to_kelvin romer_to_celsius romer
end

# doctest: converts romer to reaumur
# >> romer_to_reaumur 100
# => 140.95224000000002
def romer_to_reaumur(romer)
  celsius_to_reaumur romer_to_celsius romer
end

# doctest: converts romer to planck
# >> romer_to_planck 100
# => 3.171548964733535e-30
def romer_to_planck(romer)
  celsius_to_planck romer_to_celsius romer
end

# doctest: converts romer to rankine
# >> romer_to_rankine 100
# => 808.81254
def romer_to_rankine(romer)
  celsius_to_rankine romer_to_celsius romer
end

# doctest: converts romer to newton
# >> romer_to_newton 100
# => 58.142799000000004
def romer_to_newton(romer)
  celsius_to_newton romer_to_celsius romer
end

# doctest: converts romer to delisle
# >> romer_to_delisle 100
# => -114.28544987
def romer_to_delisle(romer)
  (celsius_to_delisle romer_to_celsius romer).round(8)
end

# ****************************DELISLE*************************************
# doctest: converts delisle to celsius
# >> delisle_to_celsius 100
# => 33.333299999983325
def delisle_to_celsius(delisle)
  (delisle / -1.49999925) + 100
end

# doctest: converts delisle to fahrenheit
# >> delisle_to_fahrenheit 100
# => 91.99993999996998
def delisle_to_fahrenheit(delisle)
  celsius_to_fahrenheit delisle_to_celsius delisle
end

# doctest: converts delisle to kelvin
# >> delisle_to_kelvin 100
# => 306.4832999999833
def delisle_to_kelvin(delisle)
  celsius_to_kelvin delisle_to_celsius delisle
end

# doctest: converts delisle to reaumur
# >> delisle_to_reaumur 100
# => 26.66663999998666
def delisle_to_reaumur(delisle)
  celsius_to_reaumur delisle_to_celsius delisle
end

# doctest: converts delisle to planck
# >> delisle_to_planck 100
# => 2.1632308360124052e-30
def delisle_to_planck(delisle)
  celsius_to_planck delisle_to_celsius delisle
end

# doctest: converts delisle to rankine
# >> delisle_to_rankine 100
# => 551.66994
def delisle_to_rankine(delisle)
  celsius_to_rankine delisle_to_celsius delisle
end

# doctest: converts delisle to newton
# >> delisle_to_newton 100
# => 10.999988999994498
def delisle_to_newton(delisle)
  celsius_to_newton delisle_to_celsius delisle
end

# doctest: converts delisle to romer
# >> delisle_to_romer 100
# => 24.999982499991248
def delisle_to_romer(delisle)
  celsius_to_romer delisle_to_celsius delisle
end

# doctest: reports from_unit to_unit and value
# >> menu_driven_report(100, "celsius", "fahrenheit")
# => "100.00 celsius is 212.00 fahrenheit"
# >> menu_driven_report(212, "fahrenheit", "celsius")
def menu_driven_report(value, from_unit, to_unit)
  format('%.2f %s is %.2f %s', value, from_unit,
         send('X_to_Y'.sub('X', from_unit).sub('Y', to_unit), value),
         to_unit)
end

# doctest: select correct option
# >> option_valid? "7"
# => true
# >> option_valid? "r"
# => false
def option_valid?(option)
  '123456789Qq'.include? option
end

# doctest: loop through each conversion
# >> convert_loop 100, "celsius"
def convert_loop(value, from_unit)
  to_units = %w(celsius fahrenheit kelvin reaumur
                planck rankine newton romer delisle)
  to_units.delete(from_unit)
  to_units.each do |to_unit|
    puts menu_driven_report(value.to_f, from_unit, to_unit)
  end
end

if __FILE__ == $PROGRAM_NAME
  puts "  Select input unit:
  ==========================
  [1] Celsius
  [2] Fahrenheit
  [3] Kelvin
  [4] Reaumur
  [5] Planck
  [6] Rankine
  [7] Newton
  [8] Romer
  [9] Delisle
  [Q] Exit
  =========================="
  condition = false
  while condition == false
    print 'Pick a number:'
    input = gets.chomp
    # see of picked is among the choices
    if option_valid? input
      if input == 'q' || input == 'Q'
        break
      else
        print 'Enter value:'
        value = gets
        if /\A[-+]?[0-9]*\.?[0-9]+\Z/.match(value)
          case input
          when '1'
            convert_loop value, 'celsius'
          when '2'
            convert_loop value, 'fahrenheit'
          when '3'
            convert_loop value, 'kelvin'
          when '4'
            convert_loop value, 'reaumur'
          when '5'
            convert_loop value, 'planck'
          when '6'
            convert_loop value, 'rankine'
          when '7'
            convert_loop value, 'newton'
          when '8'
            convert_loop value, 'romer'
          when '9'
            convert_loop value, 'delisle'
          end
        else
          puts 'Please input numeric data.'
        end
      end
    else
      puts 'Invalid choice.'
    end
  end
  exit
end
