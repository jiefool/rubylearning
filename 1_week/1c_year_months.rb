# Using what we have learned so far: I wonder if anyone could have a crack at
# working out the age in years and months form. For example Im 32 years and 6
# months old. Use the following values for age_in_seconds:
# 979000000 2158493738 246144023 1270166272 1025600095

# Constants
DAYS_MONTH = 30.4368
DAYS_YEAR = 365.242
# doctest: total number of seconds in a month with 30 days
# >> month_seconds
# => 2629739.5200000005
def month_seconds
  DAYS_MONTH * 60 * 60 * 24
end

# doctest: total number of second in a year
# >> year_seconds
# => 31556908.799999997
def year_seconds
  DAYS_YEAR * 60 * 60 * 24
end

# doctest: totals number of years in a given seconds
# >> number_of_years 979_000_000
# => 31
def number_of_years(seconds)
  (seconds / year_seconds).to_i
end

# doctest: total number of months in a given seconds
# >> number_of_months 979_000_000
# => 372
def number_of_months(seconds)
  (seconds / month_seconds).to_i
end

# doctest: return year and month age
# >> year_month_age 979_000_000
# => {:year => 31, :month => 0}
def year_month_age(seconds)
  { year: number_of_years(seconds),
    month: number_of_months(seconds) - (number_of_years(seconds) * 12) }
end

def pluralize(count, text)
  count > 1 ? text << 's' : text
end

# doctest: print age prompt
# >> print_age 1, 0
# => "1 year old."
# >> print_age 2, 0
# => "2 years old."
# >> print_age 1, 1
# => "1 year and 1 month old."
# >> print_age 2, 2
# => "2 years and 2 months old."
def age_phrase_prompt(year, month)
  text = ''
  text << "#{year} #{pluralize(year, 'year')}" if year > 0
  text << ' and ' unless year == 0 || month == 0
  text << "#{month} #{pluralize(month, 'month')}" if month > 0
  text << ' old.' unless text == ''
end

if __FILE__ == $PROGRAM_NAME
  [979_000_000,
   2_158_493_738,
   246_144_023,
   1_270_166_272,
   1_025_600_095].each do |seconds|
     age = year_month_age seconds
     puts age_phrase_prompt(age[:year], age[:month])
   end
end
