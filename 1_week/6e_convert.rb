# Write a method called convert that takes one argument which is a temperature
# in degrees Fahrenheit. This method should return the temperature in degrees
# Celsius.

# To format the output to say 2 decimal places, we can use the Kernel's format
# method. For example, if x = 45.5678 then format("%.2f", x) will return the
# string 45.57. Another way is to use the round function as follows:

# puts (x*100).round/100.0

# doctest: convert -40 in farenheit to celcius
# >> convert -40
# => -40
# doctest: convert 212 in fahrenheit to 100 celsius
# >> convert 212
# => 100
# doctest: freezing
# >> convert 32
# => 0
# doctest: Body temperature
# >> convert 98.6
# => 37
# doctest: Not quite body temperature trusted to be accurate to 0.e-4
# >> convert(98).round(4)
# => 36.6667
# doctest: converting a string causes an NoMethodError
# >> begin ; convert "a string" ; rescue => e ; e.class ; end
# => NoMethodError
# doctest: convert is named poorly, let's alias it _F_to_celsius
# >> _F_to_celsius(-40)
# => -40
def _F_to_celsius fahrenheit
  (fahrenheit - 32) * 5 / 9.0
end
alias :convert :_F_to_celsius

if __FILE__ == $0
  report_string = 'If your temperature is %.2fF it would be %.2fC.'
  [212, 72, 31, 0, -40].each do |fahrenheit|
    puts report_string % [fahrenheit, convert(fahrenheit)]
  end
end
