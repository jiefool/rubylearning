# doctest: multiplication table
# >> multiplication_table 2, '2x2', false
def multiplication_table(size = 5, heading = 'Table 1', decor = false)
  table = "#{heading}\n"
  max_digits = column_max_digits(size)
  table << decor(size, max_digits) << "\n" if decor
  table << "#{create_table size, max_digits}"
  table << decor(size, max_digits) if decor
  table
end

# doctest: create_table
# >> create_table 3, [1, 1, 1]
# => " 1  2  3 \n 2  4  6 \n 3  6  9 \n"
def create_table(size, max_digits)
  table = ''
  table << " 0 \n" unless size > 0
  1.upto(size) do |y|
    1.upto(size) do |x|
      if less_digits?(x, y, max_digits[x - 1])
        digit_difference(x, y, max_digits[x - 1]).times { table << ' ' }
      end; table << " #{y * x} "
    end; table << "\n"
  end
  table
end

# doctest: decor
# >> decor 4, [1, 1, 2, 2]
# => '=============='
def decor(size, max_digits)
  total = size * 2
  max_digits.each do |x|
    total += x
  end
  total = 3 unless total > 3
  '=' * total
end

# doctest: column max digit
# >> column_max_digits 4
# => [1, 1, 2, 2]
def column_max_digits(size)
  z = 0
  column_max_digits = []
  1.upto(size) do |y|
    1.upto(size) do |x|
      z = x
    end
    column_max_digits << (y * z).to_s.length
  end
  column_max_digits
end

# doctest: product digits
# >> less_digits? 1, 1, 2
# => true
# >> less_digits? 3, 4, 2
# => false
def less_digits?(x, y, max)
  (y * x).to_s.length < max
end

# doctest: digit diference
# digit_difference 1, 2, 2
# 1
def digit_difference(x, y, max)
  max - (y * x).to_s.length
end

if __FILE__ == $PROGRAM_NAME
  table_arguments = [[0, '0x0', true], [1, '1x1', false], [4, '4x4', true]]
  table_arguments.each do |size, heading, decor|
    puts multiplication_table(size, heading, decor)
  end
end
