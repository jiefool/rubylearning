# doctest: I can greet the world
# >> hello
# => "Hello World!"
# doctest: I can greet someone personally
# >> hello 'Jay'
# => "Hello Jay!"
# doctest: I can greet someone else personally
# >> hello 'Satish'
# => "Hello Satish!"
# doctest: I can ask if someone is there
# >> hello 'Santa Claus', '?'
# => "Hello Santa Claus?"
def hello(name = 'World', punctuation = '!')
  "Hello #{name}#{punctuation}"
end
