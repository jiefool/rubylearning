# class called Dog, that has name as an instance variable and the following
# methods:
#
#   bark(), eat(), chase_cat()
#   I shall create the Dog object as follows:
#     d = Dog.new('Leo')
#
# end
# OK, let's start by testing... right?
class Dog
  # doctest: Setup
  # >> my_dog = Dog.new('fido')
  # doctest: We have an instance variable named @name
  # >> my_dog.instance_variables
  # => [:@name]
  def initialize(name)
    @name = name
  end
  # doctest: dog will bark
  # >> my_dog.bark
  # =>'Aw aw aw'
  def bark
    'Aw aw aw'
  end

  # doctest: dog will eat
  # >> my_dog.eat
  # => 'yum yum yum'
  def eat
    'yum yum yum'
  end

  # doctest: dog will chase cat
  # >> my_dog.chase_cat
  # => 'rof rof rof'
  def chase_cat
    'rof rof rof'
  end

  # doctest: Represent Dog object like a string
  # >> /^A dog named/ =~ my_dog.to_s
  # => 0
  # beginning of what is returned
  def to_s
    "A dog named #@name"
  end
end

if __FILE__ == $PROGRAM_NAME
  puts Dog.new('Fido')
  d = Dog.new('Leo')
  [:bark, :chase_cat, :eat].each do |meth|
    puts d.send(meth)
  end
end

