# rubocop:disable Style/BlockComments
=begin
Challenge. Here's an additional challenge for all of you who already finished
Exercise1: Write a method teach_trick which allows you to teach tricks to
specific instances of the dog class like this:
=end

# doctest: Setup
# >> my_dog = Dog.new('Widiw')
class Dog
  def initialize(name)
    @name = name
  end
  # doctest: dog can learn a new trick
  # >> my_dog.respond_to? :teach_trick
  # => true
  # doctest: teach_trick(:some_trick) {"Some trick"}
  # >> my_dog.teach_trick(:some_trick) {"Some trick"}
  # => :some_trick
  # doctest: see if widiw can some_trick
  # >> my_dog.some_trick
  # => "Some trick"
  # doctest: teaching dog to dance
  # >> my_dog.teach_trick(:dance){"#@name is dancing!"}
  # => :dance
  # doctest: make the dog dance
  # >> my_dog.dance
  # => 'Widiw is dancing!'
  # doctest: Another dog... doesn't know how to dance
  # >> Dog.new('Pearl').dance
  # => "Pearl doesn't know how to dance."
  # doctest: making dog poo
  # >> my_dog.poo
  # => "Widiw doesn't know how to poo."
  def teach_trick(name, &block)
    send(:define_singleton_method, name, &block)
  end
  def method_missing(meth, *args, &block)
    p "#{@name} doesn't know how to #{meth}."
  end
end

# doctest: The methods for a dog are listed
# >> my_dog.singleton_methods
# => [:some_trick, :dance]

if __FILE__ == $PROGRAM_NAME
  d = Dog.new('Lassie')
  d.teach_trick(:dance) { "#{@name} is dancing!" }
  puts d.dance
  d.teach_trick(:poo) { "#{@name} is a smelly doggy!" }
  puts d.poo
  d2 = Dog.new('Fido')
  puts d2.dance
  d2.teach_trick(:laugh) { "#{@name} finds this hilarious!" }
  puts d2.laugh
  puts d.laugh
  d3 = Dog.new('Stimpy')
  puts d3.dance
  puts d3.laugh
end

=begin
The output should look like this:

Lassie is dancing!
Lassie is a smelly doggy!
Fido doesn't know how to dance!
Fido finds this hilarious!
Lassie doesn't know how to laugh!
Stimpy doesn't know how to dance!
Stimpy doesn't know how to laugh!
=end

